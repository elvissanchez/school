# Bug

**1. Describe the fixes that includes this MR:**

Type the description here.

**2. Provide evidences of your fixes:**

Upload images, videos, or any multimedia content here.

**3. Describe the steps to test your fixes (be sure to describe and sequence the steps in a way that is easy to understand):**

1. Type the steps here.

**4. What changes will this MR include?**

***Added***

- 

***Changed***

- 

***Removed***

- 

**5. Check the boxes below before submitting your MR:**

- [ ] I updated the version in the pom.xml file.
- [ ] I documented my additions, changes, fixes, and deletions in the version in the CHANGELOG.md file.
- [ ] I ran the tests locally and did not get any errors.
- [ ] I updated this branch from master and there are no conflicts.

**6. Links to JIRA issues related to this MR:**

- Paste the links here.

**7. FocalPoint version:**

Paste the version here.

Your contribution is always appreciated!
