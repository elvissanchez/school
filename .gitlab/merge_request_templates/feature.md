# Feature

**1. Describe the feature that includes this MR:**

Type the description here.

**2. What changes will this MR include?**

***Added***

- 

***Changed***

- 

***Removed***

- 

**3. Check the boxes below before submitting your MR:**

- [ ] I updated the version in the pom.xml file.
- [ ] I documented my additions, changes, features, and deletions in the version in the CHANGELOG.md file.
- [ ] I ran the tests locally and did not get any errors.
- [ ] I updated this branch from master and there are no conflicts.

**4. Links to JIRA issues related to this MR:**

- Paste the links here.

**5. FocalPoint version:**

Paste the version here.

Your contribution is always appreciated!
