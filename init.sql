CREATE DATABASE "school";

CREATE SCHEMA IF NOT EXISTS "principal";

CREATE TYPE "school"."principal"."WEEKDAY" AS ENUM ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');

CREATE TABLE IF NOT EXISTS "school"."principal"."educational_institution"
(
    "id"      SERIAL  NOT NULL PRIMARY KEY,
    "name"    VARCHAR NOT NULL,
    "enabled" BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."campus"
(
    "id"                      SERIAL  NOT NULL PRIMARY KEY,
    "name"                    VARCHAR NOT NULL,
    "enabled"                 BOOLEAN NOT NULL DEFAULT TRUE,
    "educational_institution" SERIAL  NOT NULL REFERENCES "school"."principal"."educational_institution" ("id")
);

CREATE TABLE IF NOT EXISTS "school"."principal"."student"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "first_name" VARCHAR NOT NULL,
    "last_name"  VARCHAR NOT NULL,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."teacher"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "first_name" VARCHAR NOT NULL,
    "last_name"  VARCHAR NOT NULL,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."group"
(
    "id"      SERIAL  NOT NULL PRIMARY KEY,
    "name"    VARCHAR NOT NULL,
    "enabled" BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."subgroup"
(
    "id"      SERIAL  NOT NULL PRIMARY KEY,
    "name"    VARCHAR NOT NULL,
    "enabled" BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."student_subgroup"
(
    "id"          SERIAL  NOT NULL PRIMARY KEY,
    "name"        VARCHAR NOT NULL,
    "student_id"  SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id"),
    "subgroup_id" SERIAL  NOT NULL REFERENCES "school"."principal"."group" ("id"),
    "enabled"     BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."group_student"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "group_id"   SERIAL  NOT NULL REFERENCES "school"."principal"."group" ("id"),
    "student_id" SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id")
);

CREATE TABLE IF NOT EXISTS "school"."principal"."campus_teacher"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "campus_id"  SERIAL  NOT NULL REFERENCES "school"."principal"."campus" ("id"),
    "teacher_id" SERIAL  NOT NULL REFERENCES "school"."principal"."teacher" ("id")
);

CREATE TABLE IF NOT EXISTS "school"."principal"."campus_student"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "campus_id"  SERIAL  NOT NULL REFERENCES "school"."principal"."campus" ("id"),
    "student_id" SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id")
);

CREATE TABLE IF NOT EXISTS "school"."principal"."student_teacher"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "student_id" SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id"),
    "teacher_id" SERIAL  NOT NULL REFERENCES "school"."principal"."teacher" ("id")
);

CREATE TABLE IF NOT EXISTS "school"."principal"."event"
(
    "id"      SERIAL  NOT NULL PRIMARY KEY,
    "weekday" "school"."principal"."WEEKDAY",
    "starts"  TIMESTAMP WITHOUT TIME ZONE,
    "ends"    TIMESTAMP WITHOUT TIME ZONE,
    "enabled" BOOLEAN NOT NULL DEFAULT TRUE,
    "subject" SERIAL  NOT NULL REFERENCES "school"."principal"."subject" (id)
);

CREATE TABLE IF NOT EXISTS "school"."principal"."subject"
(
    "id"      SERIAL  NOT NULL PRIMARY KEY,
    "name"    VARCHAR NOT NULL,
    "enabled" BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS "school"."principal"."student_subject"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "student_id" SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id"),
    "subject_id" SERIAL  NOT NULL REFERENCES "school"."principal"."subject" ("id")
);

CREATE SCHEMA IF NOT EXISTS "report_card";

CREATE TABLE IF NOT EXISTS "school"."report_card"."score"
(
    "id"         SERIAL  NOT NULL PRIMARY KEY,
    "score"      REAL    NOT NULL,
    "enabled"    BOOLEAN NOT NULL DEFAULT TRUE,
    "student_id" SERIAL  NOT NULL REFERENCES "school"."principal"."student" ("id"),
    "subject_id" SERIAL  NOT NULL REFERENCES "school"."principal"."subject" ("id")
);
